/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This file is made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/


#include "ilcplex/ilocplex.h"

#include <iostream>

int main() {

	IloEnv cplxEnv;				
	IloModel cplxModel;			
	IloObjective cplxObjective;	
	IloNumVarArray cplxVars; 	
	IloNumVar eta;	
	
	std::cout << "CPLEX seems to work..." << std::endl;
	std::cout << "Note that this is merely intended to test the repository / CMake setup and not functionality..." << std::endl;
	
	return 0;

}